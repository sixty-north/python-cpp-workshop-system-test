from setuptools import setup, Extension

################################################################################
# Import configuration
#
# We import the module `config` and expect it to expose a few attributes. Make
# sure there's a config.py (possibly copied from one of the provided templates)
# configured for your system.
#
# config.include_dirs: a list of directories where your compiler will look for
#     Boost headers.
#
# config.library_dirs: a list of directories where your compiler/linker will
#     look for the Boost libraries.
#
# config.libraries: a list containing the Boost.Python library you will link against.

import config

################################################################################
# DO NOT EDIT BELOW HERE
################################################################################

ext_module = Extension(
    'boost_python_workshop_system_test',
    sources=['system_test.cpp'],
    include_dirs=config.include_dirs,
    library_dirs=config.library_dirs,
    libraries=config.libraries)

setup(
    name='boost_python_workshop_system_test',
    description="Ensure that your system is ready for Sixty North's Boost.Python workshop",
    author='Sixty North AS',
    author_email='austin@sixty-north.com',
    ext_modules=[ext_module]
)
