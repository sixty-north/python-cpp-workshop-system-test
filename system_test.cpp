#include <Python.h>

#include <string>

#include <boost/python.hpp>

std::string check() {
    return "Your system is ready!";
}

BOOST_PYTHON_MODULE(boost_python_workshop_system_test) {
    boost::python::def("check", &check);
}
