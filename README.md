# Boost.Python workshop system test

This is a system test for Sixty North's Boost.Python workshop. This will
determine if your system has all of the requisite software, libraries, and so
forth needed to do the exercises in the workshop.

Successfully building Boost.Python modules can be tricky, and a lot of that
complexity comes from setting up the build infrastructure. It would be nearly
impossible for us to bootstrap a Boost.Python system for every student in a
workshop setting, so we ask that students use this system test to prep their
machines prior to the workshop in order to avoid delays in the workshop itself.

## Important: Save your `config.py`

To run this system test you'll need to create a file called `config.py`. This
file will contain all of your system-specific details that the workshop needs.
**Make sure you save this `config.py` and have it available at the workshop
itself!**

## The Short Version

If your system has Python, Boost.Python, and a C++ toolchain installed, then it
may be very simple to run this system test. The first thing you need to do is to
create a file called `config.py` in the same directory as this README.

There are a number of example configurations that you can start from. For
example, `OSX.config.py` is a config that is known to work on some OS X systems.
Try copying one of these examples to `config.py` to get started.

Once you've got a `config.py` in place you can run the system test:

```
$ python setup.py install -f
. . .
$ python system_test.py
Your system is ready!
```

If the last step prints "Your system is ready!" then you're all set. Otherwise,
edit `config.py` to match your system and re-run those two steps.

## Basic requirements

### Platform

This workshop is known to work on the following platforms:

- OS X
- Linux
- Windows

It's been tested on these platforms, though obviously not on every possible
version and variant of them. It *may* work on other platforms as well, but we
can't make any promises.

### Programs and libraries

Fundamentally you need three major pieces of software for this workshop:

1. **Python 3.3+** Make sure you have a working Python installation. You'll need
   both the Python executable as well as development libraries and headers.

2. **Boost** At a minimum you'll need the Boost.Python library compiled against
   the version of Python you want to use.

3. **C++ compiler/linker** This workshop involves creating C++ libraries that
   Python can load. You'll need to be able to compile C++ code and link it into
   the correct library format for your system (DLL, .so, etc.).

### Knowledge and skills

This workshop is about writing C++ code to create Python modules, and we assume
some familiarity with both languages. You need to be familiar with the C++
language and building libraries in it. You also need to be comfortable with the
Python language, its notion of modules, and its dynamic runtime behavior. You'll
be learning how to build a bridge between these two languages, so we have to
assume that you already know something about the two worlds.

## Running the system test

On many systems - especially Linux and OS X - it's relatively easy to
install Python, Boost.Python, and the C++ toolchain in such a way that
everything will "just work". Using your system's package manager will generally
give you good results.

Once you've got the necessary libraries and tools installed, you can use this
system test to verify you system. The first thing you need to do is create a
`config.py` to reflect your system. Look at the provided examples to see how it should look.

Next, you need to build and install the test module:

```
$ python setup.py install
```

Once this completes successfully, you need to test the package:

```
$ python system_test.py
Your system is ready!
```

If this prints the string "Your system is ready!" then you're ready for the
workshop. If not, then you need to look at the output of the build and test
steps to see what might be wrong. It might take some time to get your
`config.py` configuration set correctly.

## Troubleshooting

### Build on Windows

See `windows.md` for information on building Boost.Python on Windows.
