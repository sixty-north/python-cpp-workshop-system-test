# Example configuration for Debian 9 (stretch).

# To configure the system:
#
#    apt-get install g++ libboost-python-dev python3-dev

include_dirs = ['/usr/include']
library_dirs = ['/usr/lib/x86_64-linux-gnu']
libraries = ['boost_python-py35']
