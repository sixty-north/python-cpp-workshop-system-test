# Example configuration for OS X

# This is based on a system using homebrew to install Python3 and Boost.Python:
#
#    brew install python3
#    brew install boost-python --with-python3

include_dirs = ['/usr/local/include']
library_dirs = ['/usr/local/lib']
libraries = ['boost_python3']
