# Windows

Boost.Python can be challenging on Windows. You need to be very careful about
using the same version of VC++ for compiling Python, Boost.Python, and the
exercises in this workshop. If you're using a precompiled version of Python
(e.g. via one of the installers provided at python.org), then you need to make
sure you have the same version of the VC++ compiler that they used to build
Python. You can find details about which compilers were
used [on the Python wiki](https://wiki.python.org/moin/WindowsCompilers).

> For this workshop you don't necessarily need Visual Studio, but you at least
> need the standalone build tools for the correct version of the C++ compiler.

If you're using a precompiled Python and a precompiled Boost distribution, you
may have trouble getting the compiler versions correct. In my experience, it's
generally simpler to compile your own Boost.Python libary. Follow the
instructions
[at boost.org](boost.org/doc/libs/1_65_1/more/getting_started/windows.html) to
build and install the `b2` build tool.

I'm glossing over a lot of details because they're all covered in Boost's
documentation. Compiling Boost.Python on Windows has some unavoidable
complexity, but this should get you pointed in the right direction.

## Sample installation and build instructions

What follows are some reasonably detailed instructions for installing,
configuring, and building everything you need for the system test and workshop.
It assumes you're using a pre-compiled Python 3.6. Rather than use a pre-built
Boost, these instructions have you compile Boost.Python yourself.

Other combinations of versions *are* possible, but then you'll need to make sure
you align compilers, SDKs, and so forth.

### Install Python 3.6.2

The first thing to do is to install Python. You can get the Python installer
from
[the Python website](https://www.python.org/ftp/python/3.6.2/python-3.6.2.exe).
Run this and install Python into `C:\Python36`.

### Install Visual Studio 2017 Standalone Build Tools

You need at least the standalone build tools for Visual Studio 2017. You can
also use the full Visual Studio if you want.

You can download the
installer
[from Microsoft](http://landinghub.visualstudio.com/visual-cpp-build-tools). You
want the download labeled "Build Tools for Visual Studio 2017". Make sure you
select the Visual C++ 14.0 option during installation, and you may need to
install the Windows 10 SDK as well. (Microsoft seems to have gone to great pains
to make their version numbers obscure and misleading.)

### Build Boost.Python

To build Boost.Python you need to
first [download `boost_1_65_1.zip`](http://www.boost.org/users/download/). Unzip
this into `c:\boost\boost_1_65_1`.

Create the file `%HOMEDRIVE%%HOMEPATH%\user-config.jam` with these contents:

```
using python
  : 3.6
  : c:/python36/python.exe
  : c:/python36/include
  : c:/python36/libs
  ;
```

Then build `b2`, Boost's build tool:

```
cd c:\boost\boost_1_65_1\tools\build
bootstrap.bat
b2 --prefix=c:\boost\build_tool
```

Finally, build Boost.Python:

```
cd c:\boost\boost_1_65_1
c:\boost\build_tool\bin\b2 --build-dir=c:\boost\build toolset=msvc-14.0 address-model=64 architecture=x86 link=shared threading=multi runtime-link=shared release --with-python
```

If everything goes smoothly you should have the libraries you need in
`c:\boost\boost_1_65_1\stage\lib`. You'll reference this directory in your
`config.py` when building the system test and workshop modules.

### Library paths

Your Boost.Python modules will require the Boost.Python DLL when you import them
into Python. You'll need to make sure that your `PATH` environment variable
contains the directory with the DLL. If you've followed the directions above,
that directory is `c:\boost\boost_1_65_1\stage\lib`.
